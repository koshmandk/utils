import contextlib
import multiprocessing.connection
import sys
import time
import typing
from typing import Any, Callable, Generator

import IPython
import matplotlib.pyplot as plt
import pandas as pd
import psutil
import seaborn

T = typing.TypeVar


def _background_call_target(
    function: Callable[[], T],
    connection: multiprocessing.connection.Connection,
    interval: float = 0.1,
) -> None:
    results = []
    wait_time = 0
    while not connection.poll(wait_time):
        time_begin = time.time()
        result = function()
        time_end = time.time()
        results.append((result, time_end))
        wait_time = max(0, time_begin + interval - time_end)

    results.append((function(), time.time()))
    connection.send(results)


@contextlib.contextmanager
def background_call(
    function: Callable[[], T], interval: float = 0.1
) -> list[tuple[T, float]]:
    """
    Calls given function in a child process approximately
    each interval seconds. Returns the function results and
    the timestamps when they were received.

    Example:
        with background_call(psutil.Process().memory_info) as background_results:
            ...

    """
    parent_connection, child_connection = multiprocessing.Pipe()
    process = multiprocessing.Process(
        target=_background_call_target,
        kwargs=dict(
            function=function,
            connection=child_connection,
            interval=interval,
        ),
    )
    process.start()
    results = []
    try:
        yield results
        parent_connection.send(True)
        if parent_connection.poll(timeout=10 * interval):
            results.extend(parent_connection.recv())

    finally:
        process.join(timeout=interval)


def log(name: str, obj: Any) -> None:
    if isinstance(obj, plt.Figure):
        plt.close(obj)

    print(f"{name}:")
    IPython.display.display(obj)


@contextlib.contextmanager
def log_figure(title: str, seaborn_style: str = "whitegrid"):
    plt.figure()
    seaborn.set_style(seaborn_style)
    yield
    plt.title(title)
    log(title, plt.gcf())


@contextlib.contextmanager
def log_memory_usage(
    name: str | None = None, interval: float = 0.1, unit: str = "GiB"
) -> Generator[None, None, None]:
    # TODO: turn into ContextDecorator and take the function name.

    unit_size = {"MiB": 1 << 20, "GiB": 1 << 30}.get(unit)
    if unit_size is None:
        raise ValueError(f"Unrecognized memory size unit {unit}.")

    with background_call(psutil.Process().memory_info, interval) as background_results:
        yield

    title = f'"{name}" memory profile in {interval:.2f}s intervals'
    if not background_results:
        log(
            title,
            "Memory logs are missing, possibly because the interval is "
            "too short or because the background function has timed out.",
        )
        return

    df = pd.DataFrame(
        {"time": pd.to_datetime(timestamp, unit="s")}
        | {f"process {k}": v / unit_size for k, v in memory_info._asdict().items()}
        for memory_info, timestamp in background_results
    ).set_index("time")

    with log_figure(title):
        axis = seaborn.lineplot(df)
        axis.tick_params(axis="x", labelrotation=15)
        axis.set(ylabel=unit)


@contextlib.contextmanager
def time_it(name: str = "", file=sys.stderr) -> None:
    begin = time.perf_counter()
    yield
    end = time.perf_counter()
    delta_ms = 1000 * (end - begin)
    name = name + ":\t" if name else ""
    print(f"{name}{delta_ms:.3f} ms", file=file)
