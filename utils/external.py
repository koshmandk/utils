import contextlib
import json
import os
import subprocess
import tempfile
import zipfile
from typing import Generator


def unzip(zip_path: str, save_path: str | None = None, delete: bool = False) -> None:
    with zipfile.ZipFile(zip_path) as f:
        f.extractall(path=save_path)
    if delete:
        os.remove(zip_path)


def unzip_in_directory(directory: str, delete: bool = False) -> None:
    for path in os.listdir(directory):
        path = os.path.join(directory, path)
        if zipfile.is_zipfile(path):
            unzip(path, save_path=directory, delete=delete)


def capture_stdout(command: str) -> str:
    completed_process = subprocess.run(command.split(), capture_output=True)
    stdout = completed_process.stdout.decode()
    return stdout


@contextlib.contextmanager
def open_tempfiles(
    n_tempfiles: int,
) -> Generator[list[tempfile._TemporaryFileWrapper], None, None]:
    opened_files = []
    with contextlib.ExitStack() as stack:
        for _ in range(n_tempfiles):
            opened_file = stack.enter_context(tempfile.NamedTemporaryFile())
            opened_files.append(opened_file)
        yield opened_files


def resolve_package_versions(
    requirements,
    dry_run: bool = False,
    quiet: bool = False,
    ignore_installed: bool = False,
    save_to_requirements_txt: bool = False,
) -> str:
    """
    Some packages require cmake:
    `brew install cmake`
    and some require the `wheel` package.
    """
    with tempfile.NamedTemporaryFile() as requirements_file:
        with tempfile.NamedTemporaryFile() as report_file:
            requirements_file.write(bytes(requirements, "utf-8"))
            requirements_file.flush()

            arguments = [
                "pip",
                "install",
                "-r",
                requirements_file.name,
                "--report",
                report_file.name,
            ]
            if dry_run:
                arguments.append("--dry-run")
            if quiet:
                arguments.append("--quiet")
            if ignore_installed:
                arguments.append("--ignore-installed")

            subprocess.run(arguments)
            report = json.load(report_file)

    resolved_package_versions = []
    for package in report["install"]:
        package_name = package["metadata"]["name"]
        package_version = package["metadata"]["version"]
        resolved_version = f"{package_name}=={package_version}"
        resolved_package_versions.append(resolved_version)

    resolved_package_versions = "\n".join(sorted(resolved_package_versions))

    if save_to_requirements_txt:
        with open("requirements.txt", "w") as f:
            f.write(resolved_package_versions)

    return resolved_package_versions

def build_vscode_extension_download_link(
    publisher:str = "ms-python",
    extension:str = "python",
    version:str = "2022.16.1",
) -> str:
    link = (
        f"https://marketplace.visualstudio.com/_apis/public/gallery/"
        f"publishers/{publisher}/"
        f"vsextensions/{extension}/"
        f"{version}/vspackage"
    )
    return link
