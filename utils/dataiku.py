import contextlib
import re
import shutil
import tempfile
from typing import Callable

import dataiku
import dataikuapi


def get_dataiku_cloud_code_environment_path() -> str:
    return "/opt/dataiku/code-env"


def get_dataiku_client() -> dataikuapi.DSSClient:
    try:
        import dataiku

        client = dataiku.api_client()
    except ImportError:
        client = dataikuapi.DSSClient(host="http://localhost:11200")

    return client


def get_dataiku_user_secrets() -> dict:
    auth_info = get_dataiku_client().get_auth_info(with_secrets=True)
    secrets = auth_info["secrets"]
    secrets = {secret["key"]: secret["value"] for secret in secrets}
    return secrets


def update_standard_variables(
    standard_variables: dict | None = None, local_variables: dict | None = None
) -> None:
    project = get_dataiku_client().get_default_project()
    variables = project.get_variables()
    if standard_variables is not None:
        variables["standard"].update(standard_variables)
    if local_variables is not None:
        variables["local"].update(local_variables)
    project.set_variables(variables)


def search_for_packages_in_environments(
    *packages,
) -> list[tuple[dataikuapi.dss.admin.DSSCodeEnv, str, str]]:
    client = get_dataiku_client()
    result = []
    for env in client.list_code_envs(as_objects=True):
        env_packages = env.get_definition()["actualPackageList"]
        if all(re.findall(package, env_packages) for package in packages):
            result.append((env, env.env_name, env_packages))

    return result


def download_file_from_folder(
    folder: dataiku.Folder, path: str
) -> tempfile._TemporaryFileWrapper:
    file = tempfile.NamedTemporaryFile()
    with folder.get_download_stream(path) as input_stream:
        shutil.copyfileobj(input_stream, file)

    file.seek(0)
    return file


def get_numpy_types_to_dataiku() -> dict[str, str]:
    return {
        "object": "string",
        "bool": "boolean",
        "float64": "double",
        "int64": "bigint",
    }


def does_dataset_exist(
    project: dataikuapi.dss.project.DSSProject, dataset_name: str
) -> bool:
    datasets = project.list_datasets(as_type="objects")
    exists = dataset_name in {ds.name for ds in datasets}
    return exists


def get_project_managed_folders(
    project: dataikuapi.dss.project.DSSProject, by_name: bool = False
) -> dict:
    folders = {
        mf["id"]: project.get_managed_folder(mf["id"])
        for mf in project.list_managed_folders()
    }
    if by_name:
        folders = {mf.get_definition()["name"]: mf for mf in folders.values()}
    return folders


def get_project_notebooks(project: dataikuapi.dss.project.DSSProject) -> dict:
    notebooks = {nb.notebook_name: nb for nb in project.list_jupyter_notebooks()}
    return notebooks


def get_project_recipes(project) -> dict:
    recipes = {r.recipe_name: r for r in project.list_recipes(as_type="object")}
    return recipes


@contextlib.contextmanager
def update_settings(obj):
    settings = obj.get_settings()
    yield settings
    settings.save()

def get_dataset_columns(dataset: str) -> list[str]:
    dataset = dataiku.Dataset(dataset)
    columns = [c["name"] for c in dataset.get_config()["schema"]["columns"]]
    return columns

def build_rename_columns_step(dataset: str, rename = lambda x: x.lower().replace(" ", "_")) -> str:
    columns = get_dataset_columns(dataset)
    renaming = "\n".join(c + "\t" + rename(c) for c in columns)
    return renaming

def modify_dataset_column_schema(dataset_name:str, modify: Callable[[dict], [dict]]) -> None:
    client = dataiku.api_client()
    project = client.get_default_project()
    dataset = project.get_dataset(dataset_name)
    schema = dataset.get_schema()
    schema["columns"] = [modify(c) for c in schema["columns"]]
    dataset.set_schema(schema)
    

def query_item(executor, query):
    item = executor.query_to_df(query).values.item()
    return item

def get_table_in_schema(table_schema, table_name):
    table_in_schema = f'"{table_schema}"."{table_name}"'
    return table_in_schema

def describe_connections(sql_connections = ('BigQuery', 'Greenplum', 'Oracle', 'PostgreSQL', 'SQLServer')) -> pd.DataFrame:
    client = dataiku.api_client()
    connections = []
    for connection_name in tqdm.tqdm(client.list_connections_names("all")):
        connection = client.get_connection(connection_name)
        connection_type = connection.get_info()["type"]
        connection_info = None
        database = None
        n_schemas = 0
        n_tables = 0
        
        if connection_type in sql_connections:
            executor = dataiku.core.sql.SQLExecutor2(connection=connection_name)
            if connection_type == "Oracle":
                query_version = lambda: query_item(executor, "SELECT banner FROM v$version")
            elif connection_type == "SQLServer":
                query_version = lambda: executor.query_to_df("SELECT @@version").columns[0]
            else:
                query_version = lambda: query_item(executor, "SELECT VERSION()")
                
            try:
                connection_info = query_version()
            except Exception as exception:
                message = str(exception).lstrip("None: b").strip('"')
                if "Error getting access token" in message or "does not have credentials" in message:
                    connection_info = "No access"
                elif "Function not found" in message:
                    connection_info = None
                else:
                    connection_info = message
                
            if connection_info and connection_info != "No access":
                if connection_type == "Oracle":
                    database = query_item(executor, "SELECT ora_database_name FROM dual")
                elif connection_type == "SQLServer":
                    database = executor.query_to_df("SELECT db_name()").columns[0]
                else:
                    database = query_item(executor, "SELECT current_database()")
                
                sql_description = describe_sql_connection(executor)
                n_schemas = sql_description["table_schema"].nunique()
                n_tables = len(sql_description)
            
        connections.append((connection_name, connection_type, connection_info, database, n_schemas, n_tables))
        
    columns = ["connection_name", "connection_type", "connection_info", "database", "n_schemas", "n_tables"]
    connections = pd.DataFrame(
        connections, 
        columns=columns,
    ).sort_values(columns[:2]).reset_index(drop=True)
    return connections


def get_connection_type(executor):
    connection_type = dataiku.api_client().get_connection(executor._iconn).get_info()["type"]
    return connection_type

def describe_sql_connection(executor, compute_table_metrics: bool = False) -> pd.DataFrame:
    if get_connection_type(executor) == "Oracle":
        non_sys_schemas = tuple(set(executor.query_to_df('SELECT owner FROM all_tables')["OWNER"]) - set(['CTXSYS', 'MDSYS', 'SYS', 'SYSTEM', 'XDB', 'WMSYS']))
        df = executor.query_to_df(f"SELECT owner, table_name, column_name FROM all_tab_cols WHERE OWNER IN {non_sys_schemas}")
        df = df.rename({"OWNER": "table_schema"}, axis="columns")
        df.columns = [c.lower() for c in df.columns]
    else:
        df = executor.query_to_df("SELECT table_schema, table_name, column_name FROM information_schema.columns")
    
    df = df.groupby(["table_schema", "table_name"])["column_name"].agg(list).reset_index().rename(
        {"column_name": "column_names"},
        axis="columns",
    )
    df["column_count"] = df["column_names"].str.len()
    
    if compute_table_metrics:        
        row_counts = []
        for _, row in df.iterrows():
            table_in_schema = get_table_in_schema(row["table_schema"], row["table_name"])
            row_count = query_item(executor, f"SELECT COUNT(*) FROM {table_in_schema}")
            row_counts.append(row_count)
        df["row_count"] = row_counts
            
    return df

def describe_table_columns(executor, table_schema, table_name) -> pd.DataFrame:
    if get_connection_type(executor) != "Oracle":
        df = executor.query_to_df(
            f"SELECT * FROM information_schema.columns WHERE table_schema = '{table_schema}' AND table_name = '{table_name}'"
        ).drop(["table_catalog", "table_schema", "table_name"], axis="columns")
    else:
        non_sys_schemas = tuple(set(executor.query_to_df('SELECT owner FROM all_tables')["OWNER"]) - set(['CTXSYS', 'MDSYS', 'SYS', 'SYSTEM', 'XDB', 'WMSYS']))
        df = executor.query_to_df(f"SELECT * FROM all_tab_cols WHERE OWNER IN {non_sys_schemas}")
        df.columns = [c.lower() for c in df.columns]
    return df

def limit_rows(executor, n_rows):
    limit = limit = f"FETCH FIRST {n_rows} ROWS ONLY" if get_connection_type(executor) == "Oracle" else f"LIMIT {n_rows}"
    return limit

def do_have_table_permission(executor, table_schema, table_name) -> bool:
    table_in_schema = get_table_in_schema(table_schema, table_name)
    limit = limit_rows(executor, 1)
    try:
        executor.query_to_df(f"SELECT * FROM {table_in_schema} {limit}")
    except Exception as exception:
        if "permission denied" in str(exception).lower():
            return False
        raise exception
    return True

