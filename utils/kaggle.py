import json
import os


def setup_kaggle():
    if "KAGGLE_KEY" not in os.environ:
        from .dataiku import get_dataiku_user_secrets

        os.environ["KAGGLE_USERNAME"] = "dimakoshman"
        os.environ["KAGGLE_KEY"] = get_dataiku_user_secrets()["kaggle_key"]


def save_kaggle_api_key(api_token: dict, kaggle_directory: str = "~/.kaggle") -> None:
    """
    Given the api token downloaded from https://www.kaggle.com/settings/account,
    saves it, allowing to use the kaggle cli. Tokens expire quite often, and without
    them kaggle may throw unauthorized access errors.
    """

    kaggle_directory = os.path.expanduser(kaggle_directory)
    os.makedirs(kaggle_directory, exist_ok=True)
    api_token_json = os.path.join(kaggle_directory, "kaggle.json")
    with open(api_token_json, "w") as file:
        json.dump(api_token, file)

    os.chmod(api_token_json, 0o600)
